package demo;
//记根root所在的层数为1 前序遍历时同时打印对应的高度
public class Solution {
    public void preorderWithLevel(TreeNode root,int level){
        if(root==null){
            return ;
        }
        System.out.println(root.val+" "+level);
        preorderWithLevel(root.left,level+1);
        preorderWithLevel(root.right,level+1);
    }
}
