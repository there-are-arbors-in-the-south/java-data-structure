package demo;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if(root==null){
            return list;
        }
        list.addAll(postorderTraversal(root.left));//左子树
        list.addAll(postorderTraversal(root.right));//右子树
        list.add(root.val);//根
        return list;

    }
}
