package demo;
public class Solution {
    public static int  maxElement(int[]array){
        int max=array[0];

        for(int i=1;i<array.length;i++){
            if(array[i]>max){
                max=array[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[] a = {1, 5, 6, 88, 45, 21, -2, 0};
        System.out.println(maxElement(a));
    }
    }
