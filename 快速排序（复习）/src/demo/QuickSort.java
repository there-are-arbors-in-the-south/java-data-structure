package demo;

import java.util.Arrays;

public class QuickSort {

    public static void quickSort(long[]array){
        int size=array.length;

        for(int i=1;i<size;i++){

            long key=array[i];
            int j=i-1;
            for(j=i-1;j>=0&&key<array[j];j--){
                array[j+1]=array[j];
            }
            array[j+1]=key;
        }
    }

    public static void main(String[] args) {
        long[]arr={1,5,4,9,88,5,6,4,1};
        quickSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
