package demo;
import java.util.Stack;
public class Solution {
    public static void main(String[] args) {
        Stack<Integer> stack=new Stack<>();
        //压栈
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        //取出栈顶元素
        Integer a=stack.pop();
        System.out.println(a);
        //查看栈顶元素
        Integer b=stack.peek();
        System.out.println(b);
        //获取栈中有效元素个数
        Integer c=stack.size();
        System.out.println(c);
        //检测栈里是否为空
        boolean d=stack.isEmpty();
        System.out.println(d);
    }
}
