package demo;
//让一张扑克牌具有比较能力
public class Card {
    private int rank;//数值
    private String suit;//花色

    public Card(int rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    @Override
    public boolean equals(Object obj) {
        //自己和自己比较
       if(this==obj){
           return true;
       }
       //对传入的对象分析
        //若传入的对象为空或者不是Card
       if(obj==null||!(obj instanceof Card)){
           return false;
       }

       //若传入的是Card
        Card card=(Card) obj;
        return this.rank==card.rank&&card.equals(this.suit);
    }

    public static void main(String[] args) {
        Card card1=new Card(99,"♦");
        Card card2=new Card(99,"♠");
        Card card3=new Card(50,"♦");

        System.out.println(card1.equals(card2));
        System.out.println(card1.equals(card3));
        System.out.println(card2.equals(card3));
        System.out.println(card1.equals(card1));
        System.out.println(card2.equals(card2));
        System.out.println(card3.equals(card3));
    }
}
