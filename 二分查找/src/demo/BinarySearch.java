package demo;

public class BinarySearch {
    public static int binarySearch(int[]array,int target){
        int leftIdx=0;
        int rightIdx=array.length-1;
        while(leftIdx<=rightIdx){
            int midIdx=(leftIdx+rightIdx)/2;
            if(array[midIdx]==target) {
                return 1;
            }else if (array[midIdx]<target){
                leftIdx=midIdx+1;

            }else if(array[midIdx]>target){
                rightIdx=midIdx+1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[]arr={1,2,3,9,4,5,6};
        int a=binarySearch(arr, 9);
        System.out.println(a);
    }
}
