package demo;
import java.util.Comparator;
import java.util.PriorityQueue;

public class UsePriorityQueue {
        static class ReverseIntegerComparator implements Comparator<Integer> {
            //重新实现Comparator比较器接口
            //自己定义大小规则
            @Override
            public int compare(Integer o1,Integer o2){
                int cmp=o1-o2;
                if(cmp>0){
                    return -1;
                }else if(cmp==0){
                    return 0;
                }
                else{
                    return 1;
                }
            }

        }

    public static void main2(String[] args) {
        //Integer已经实现了Comparable接口 所以可以不用传入比较器
        PriorityQueue<Integer> priorityQueue=new PriorityQueue<>();//调用无参构造方法则不用传入比较器

        priorityQueue.offer(5);
        priorityQueue.offer(4);
        priorityQueue.offer(2);
        priorityQueue.offer(8);
         //官方默认的优先级队列是最小根堆
        while(!priorityQueue.isEmpty()){
            System.out.println(priorityQueue.poll());
        }
    }

    public static void main(String[] args) {
          //创建自己定义的比较器对象
        ReverseIntegerComparator ric=new ReverseIntegerComparator();
        //检测比较器对象是否正确
         int cmp =  ric.compare(1,2);
         if(cmp>0){
             System.out.println("1大于2");
         }else if(cmp==0){
             System.out.println("1等于2");
         }else{
             System.out.println("小于2");
         }
        //如果要变成最大堆，可以自己定义一个满足大堆比较器，传入优先级队列
        PriorityQueue<Integer> priorityQueue1=new PriorityQueue<>(ric);
        priorityQueue1.offer(5);
        priorityQueue1.offer(4);
        priorityQueue1.offer(2);
        priorityQueue1.offer(8);
        //此处自己实现了最小根堆
        while(!priorityQueue1.isEmpty()){
            System.out.println(priorityQueue1.poll());
        }
    }
}
