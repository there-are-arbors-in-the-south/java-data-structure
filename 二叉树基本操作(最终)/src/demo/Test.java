package demo;

public class Test {
    public static void main(String[] args) {
        TreeNode root=CreateTree.createTree();

        CreateTree tree=new CreateTree();
        tree.preTraverdsal(root);
        System.out.println();
        tree.inorderTraverdsal(root);
        System.out.println();
        tree.postTraverdsal(root);
        System.out.println();
        System.out.println(tree.size1(root));
        System.out.println();
        System.out.println(tree.size2(root));
        System.out.println();
        System.out.println(tree.leafSize1(root));
        System.out.println();
        System.out.println(tree.leafSize2(root));
        System.out.println();
        System.out.println(tree.kLeafSize(root,2));
        System.out.println();
        System.out.println(tree.contain(root,5));
        System.out.println();
        System.out.println(tree.nodeOf(root,3));
        System.out.println();
        tree.levelOrder(root);
        System.out.println();
        tree.levelWithNull(root);
        System.out.println();
        System.out.println(tree.isCompleteTree(root));


    }


}
