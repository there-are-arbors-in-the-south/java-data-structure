package demo;
import java.util.*;
public class Main {
    public static void main(String[] args) {

        Set<Integer> set = new TreeSet<>();
        //set集合里元素需要具有比较的能力
        //Comparable 或者Comparator
        System.out.println(set.size());//0
        System.out.println(set.isEmpty());//true

        //集合中插入元素
        set.add(18);
        set.add(99);
        set.add(45);
        set.add(55);

        System.out.println(set.size());//4
        System.out.println(set.isEmpty());//false



        Iterator<Integer> it=set.iterator();//返回迭代器对象
        //TreeSet内部默认为中序遍历 数值从小到大
        while(it.hasNext()){
            Integer e=it.next();
            System.out.println(e);
            // 18 45 55 99
        }
        System.out.println("---------------");
        //每个元素都迭代
        for(Integer e:set){
            System.out.println(e);
        }
        System.out.println("------------------");
        //set里是Integer 变int自动拆箱
        for(int e:set){
            System.out.println(e);
        }

        //一个整数集合 找出集合中的偶数放到另一个集合中

        Set<Integer> set2=new TreeSet<>();
        Set<Integer> input=new TreeSet<>();
        set2.add(2);
        set2.add(8);
        set2.add(1);
        set2.add(79);
        set2.add(5);

        for(int e:set){
            if(e%2==0){
                input.add(e);
            }
        }
        System.out.println("---------------");
        //一个整数的集合 每个元素乘以2后放到另一个集合中

        Set<Integer> set3=new TreeSet<>();
        Set<Integer> input2=new TreeSet<>();
        set2.add(2);
        set2.add(8);
        set.add(1);
        set.add(79);
        set2.add(5);

        for(int e:set3){
            int o=e*2;
            input2.add(o);
        }
    }

    
}



