package demo;
//栈的模拟实现
import java.util.Arrays;
public class MyStack {

    public int[]elem;//栈基于数组实现
    public int usedSize;//栈存储的有效数据元素

    public MyStack(){
        elem=new int[10];//栈的初始空间大小
    }
    //判断栈是否满
    public  boolean isFull(){
        return usedSize==elem.length;
    }
    //压栈
    public void push(int val){
        //扩容
        if(isFull()){
          elem=Arrays.copyOf(elem,elem.length*2);
        }
        elem[usedSize]=val;
        usedSize=usedSize+1;
    }
    //出栈
    public int pop(){
        if(isEmpty()){
            throw new EmptyException("栈是空的！");
        }
        int val=elem[usedSize-1];
        usedSize=usedSize-1;
        return val;
    }
    //判断栈是否是空
    public boolean isEmpty(){
        return usedSize==0;
    }
    //查看栈顶元素
    public int peek(){
        if(isEmpty()){
            throw new EmptyException("栈是空的！");
        }
        return elem[usedSize-1];
    }
    //获取栈中的有效数据元素个数
    public int size(){
        if(isEmpty()){
            throw new EmptyException("栈是空的！");
        }
        return usedSize;
    }
}
