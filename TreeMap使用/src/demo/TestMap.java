package demo;

import java.util.Map;
import java.util.TreeMap;

public class TestMap {

    public static void main(String[] args) {

        Map<String,String> map=new TreeMap<>();

        map.put("李逵","黑旋风");
        map.put("宋江","及时雨");
        map.put("鲁智深","花和尚");
         //打印key-value关系
        System.out.println(map);
        //打印键值对个数
        System.out.println(map.size());

        String str=map.put("林冲","豹子头");
        System.out.println(str);
        //插入键值对以后如果原本map中不存在，那么插入map,返回null

        //key不能为空，value可以为空
        str=map.put("不能为空",null);
        System.out.println(map.size());
        //长度增加，说明value为空也插入成功

        System.out.println(map.get("鲁智深"));
        //get(key) 若key存在，返回相应的value 不存在返回null

        System.out.println(map.getOrDefault("阿牛","哈哈哈"));
        System.out.println(map.getOrDefault("鲁智深","花和尚"));
        //若key存在，返回对应的value
        //不存在 返回默认值

        System.out.println(map.containsKey("林冲"));
        //是否包含key

        System.out.println(map.containsValue("及时雨"));
        //是否包含value

        //打印所有的key
        for(String s:map.keySet()){
            System.out.println(s);
        }

        //打印所有的value
        for(String d:map.values()){
            System.out.println(d);
        }

        for(Map.Entry<String,String> entry:map.entrySet()){
            System.out.println(entry.getKey()+"---"+entry.getValue());
        }


    }
}
