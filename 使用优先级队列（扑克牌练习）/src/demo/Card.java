package demo;
import java.util.PriorityQueue;
//如果牌数不同按牌数比较，如果牌数相同按花色比较
public class Card implements Comparable<Card>{

    private String suit;//花色 假设 ♥>♦>♠>♣
    private int rank ;//1-13 牌数  2<3<4.....<J<Q<K<A (将1转换成A)
    //构造方法
    public Card(String suit, int rank) {
        this.suit = suit;
        this.rank = rank;
    }
    //重写Comparable的抽象方法compareTo
    @Override
    public int compareTo(Card o) {
        int thisRank=tranRank();//将此类内的牌数转换
        int oRank=o.tranRank();//将传入对象的牌数转换
        int thisSuit=tranSuit();//将此类内的花色转换
        int oSuit=o.tranSuit();//将传入对象的花色转换
        //牌数不同按牌数比较
        if(thisRank>oRank){
            return -1;
        }
        if(thisRank<oRank){
            return 1;
        }
        //如果牌数相同按花色比较
        if(thisRank==oRank){
            if(thisSuit<oSuit){
                return -1;
            }

            if(thisRank>oRank){
                return 1;
            }
        }
      return 0;
    }
    //转换牌数，令牌数1大于3，4，5~~~~等数
    public int tranRank(){
        if(this.rank==1){
            return 14;
        }
        return this.rank;
    }
    //转换花色，将花色转换成数字比较
    public int tranSuit(){
        if(this.suit=="♥"){
            return 0;
        }
        if(this.suit=="♦"){
            return 1;
        }
        if(this.suit=="♥"){
            return 2;
        }
        if(this.suit=="♣"){
            return 3;
        }
        return -1;
    }
  //toString()方法显示对象的牌数
    @Override
    public String toString() {
        return "Card{" +
                "suit='" + suit + '\'' +
                ", rank=" + rank +
                '}';
    }

    public static void main(String[] args) {
        PriorityQueue<Card> pq=new PriorityQueue<>();
        pq.offer(new Card("♥",5));
        pq.offer(new Card("♣",5));
        pq.offer(new Card("♦",1));
        pq.offer(new Card("♠",8));

        while(!pq.isEmpty()){
            System.out.println(pq.poll());
        }
        //结果：
        //Card{suit='♦', rank=1}
        //Card{suit='♠', rank=8}
        //Card{suit='♥', rank=5}
        //Card{suit='♣', rank=5}


    }


}
