package demo;

import java.util.Arrays;

public class Sorting {
    //定义交换两个整数的交换方法
    private static void swap(int[] arr,int a,int b){
        int temp=arr[a];
        arr[a]=arr[b];
        arr[b]=temp;
    }
    //冒泡排序[0,size)的所有数字
    public static void sorting(int[]arr,int size){
        //外层表示要冒泡的次数 最后一轮必定有序 冒泡size-1次
        for(int i=0;i<size-1;i++){
            //找无序区间 [0,size-i)
            //找有序区间 [size-i,size)

            boolean sort=true;
            //遍历无序区间 两两比较(遍历从0号下标开始则只能到倒数第二个，因为它要与倒数第一个比较，倒数第一个不能越界)
            for(int j=0;j<size-i-1;j++){
                if(arr[j]>arr[j+1]){
                   swap(arr,j,j+1);
                    sort=false;
                }
            }
            if(sort==true){
                return ;
            }
        }
        return ;
    }
    //对某个范围内进行冒泡排序
    public static void bubbleSort(int[]arr,int fromIndex,int toIndex){
        //需要减1，最后一轮必定有序
        for(int i=fromIndex;i<toIndex-1;i++){
            //无序区间 [formIndex,formIndex+toIndex-i  )
            //有序区间 [ formIndex+toIndex-i,toIndex)
            boolean sort=true;
            for(int j=fromIndex;j<fromIndex+toIndex-i-1;j++){
                if(arr[j]>arr[j+1]){
                    swap(arr,j,j+1);
                    sort=false;
                }
            }
            if(sort==true){
                return ;
            }
        }

    }

    public static void main(String[] args) {
        int[]arr={1,8,2,5,7,4};
       bubbleSort(arr,3,arr.length);
        System.out.println(Arrays.toString(arr));
        sorting(arr,arr.length);
        System.out.println(Arrays.toString(arr));
    }
}
