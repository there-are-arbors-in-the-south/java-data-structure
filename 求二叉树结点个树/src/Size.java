public class Size {

//求一颗数的全部结点个数
    public static int size(TreeNode root){
        if(root==null){
            //如果是空树，返回0
            return 0;
        }
        int leftHeight=size(root.left);
        int rightHeight=size(root.right);
     //如果不是空树，本次返回左子树的个数，加右子树的个数，再再加上本次的1个
        return leftHeight+rightHeight+1;
    }
    //求一颗数的叶子结点个数

    public static int size_of(TreeNode root){
        //如果树为空。叶子结点个数0
        if(root==null){
            return 0;
        }
        //没有左右孩子，则该结点是叶子结点
        if(root.left==null&&root.right==null){
            return 1;
        }
        int leftSize=size_of(root.left);
        int rightSize=size_of(root.right);

        return leftSize+rightSize;



    }


    public static void main(String[] args) {
        TreeNode root= BuildTree.buildTree();
        int num=size(root);
        System.out.println("树的结点个数:"+num);
        int num2=size_of(root);
        System.out.println("树的叶子结点个数："+num2);
    }
}
