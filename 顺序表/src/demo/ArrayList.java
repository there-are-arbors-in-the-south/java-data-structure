package demo;
//顺序表的简单实现与测试
public class ArrayList {
    private Long[]array;
    private int size;
    public ArrayList(){
        array=new Long[20];//引用类型，初始值为null
        size=0;
    }
    //尾插
    public boolean add(Long e){
        //不考虑扩容时
        array[size]=e; //[a,b] size=2
        size=size+1;//[a,b,z] size=3
        return true;
    }
    //指定下标插入元素
    public void add(int index,Long e){
        //若下标不合法抛出异常
        if(index<0||index>size){
            throw new ArrayIndexOutOfBoundsException(index);
        }
        //暂不考虑扩容 代表原位置
        for (int i=size-1;i>=index;i--){
            array[i+1]=array[i];
        }
        array[index]=e;
        size=size+1;
    }
    //删除指定下标元素
    public Long remove(int index){
        Long e=array[index];
        if (index<0||index>=size){
            throw new ArrayIndexOutOfBoundsException(index);
        }
        for(int i=index+1;i<=size-1;i++){
            array[i-1]=array[i];
        }
        array[size-1]=null;
        size=size-1;
        return e;
    }
    //删除指定元素
    public boolean remove(Long e){
        for (int i=0;i<=size;i++)
        if(array[i].equals(e)){
            remove(i);
            return true;
        }
        return false;
    }
    //获取到某下标的元素
    public Long get(int index){
        if(index<0||index>=size){
            throw new ArrayIndexOutOfBoundsException(index);
        }
        return array[index];
    }
    //设置某下标的值
    public Long set(int index,Long e){
        if(index<0||index>size){
            throw new ArrayIndexOutOfBoundsException(index);
        }
        Long o=array[index];
        array[index]=e;
        return o;
    }
    //清空顺序表
    public void clear(){
        for (int i=0;i<size;i++){
            array[i]=null;
        }
        size=0;
    }
    //返回指定元素的下标
    public int indexOf(Long e){
        for (int i=0;i<size;i++){
            if(array[i].equals(e)){
                return i;
            }
        }
        return -1;
    }
    public int lastIndexOf(Long e){
        for (int i=size-1;i>=0;i--){
            if(array[i].equals(e)){
                return i;
            }
        }
        return -1;
    }
    public boolean contains(Long e){
        return indexOf(e)!=-1;
    }
    public int size(){
        return size;
    }
    public boolean isEmpty(){
        return size==0;
    }

    public void 检查顺序表是否是一个合法的顺序表对象(){
        if(size<0){
            throw new RuntimeException("size小于0！");
        }
        if(array==null){
            throw new RuntimeException("array为null!");
        }
        if(size>array.length){
            throw new RuntimeException("size大于array.length!");
        }
        for (int i=0;i<size;i++){
            if(array[i]==null){
                throw new RuntimeException("array[0,size)出现了null");
            }
        }
        for(int i=size;i<array.length;i++){
            if(array[i]!=null){
                throw new RuntimeException("array[size,capacity)出现null！");
            }
        }

    }
    //检查元素情况
    public void 检查元素情况(long[] expectElements){
        //检查长度
        if(size!=expectElements.length){
            throw new RuntimeException("当前size不符合预期size"+size);
        }
        //检查元素与预期元素
        for (int i=0;i<size;i++ ){
            if(array[i]!=expectElements[i]){
                throw new RuntimeException("元素和预期的元素不符！");
            }
        }
    }

    public static void main(String[] args) {
        ArrayList list=new ArrayList();
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[0]);//期待一开始元素是0个
        System.out.println(list.isEmpty());

        list.add(100L);
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{100L});

        list.add(200L);
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{100L,200L});

        list.add(300L);
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{100L,200L,300L});

        list.add(0,500L);
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{500L,100L,200L,300L});

        list.add(list.size(),600L);
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{500L,100L,200L,300L,600L});

        list.add(3,700L);
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{500L,100L,200L,700L,300L,600L});
        System.out.println(list.isEmpty());

        long e=list.remove(0);
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{100L,200L,700L,300L,600L});
        System.out.println(e);

        e=list.remove(list.size()-1);
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{100L,200L,700L,300L});
        System.out.println(e);

        e=list.remove(2);
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{100L,200L,300L});
        System.out.println(e);

        list.clear();
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[0]);
        System.out.println(list.isEmpty());

        list.add(1000L);
        list.add(2000L);
        list.add(3000L);
        list.add(4000L);
        list.add(5000L);
        list.add(6000L);
      //[1000,2000,3000,4000,5000,6000]
        System.out.println(list.indexOf(1000L));;
        System.out.println(list.lastIndexOf(3000L));

        System.out.println(list.remove(2000L));
        list.检查顺序表是否是一个合法的顺序表对象();
        list.检查元素情况(new long[]{1000L,3000L,4000L,5000L,6000L});


    }
}
