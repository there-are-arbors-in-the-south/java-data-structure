package demo;
//二叉树结点类
public class TreeNode {
    public int val;//数据域
    public TreeNode left;//左孩子引用
    public TreeNode right;//右孩子引用

    public TreeNode(int val) {
        this.val = val;
    }
}
