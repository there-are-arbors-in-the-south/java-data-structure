package demo;
public class Solution {
    public boolean isMirror(TreeNode p, TreeNode q){
        //都是空树互为镜像
        if(p==null&&q==null){
            return true;
        }
        //有一颗树时空树不是互为镜像
        if(p==null||q==null){
            return false;
        }
        //判断根+左子树+右子树的总结果
        return (p.val==q.val)&&(isMirror( p.left,q.right))&&(isMirror( p.right,  q.left));
    }
}

