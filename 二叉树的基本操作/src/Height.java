public class Height {

    public static int height_of(TreeNode root){
        if(root==null){
            return 0;
        }

        int left_Height=height_of(root.left);
        int right_Height=height_of(root.right);
        //本次递归的高度+已经递归得到的左右子树里更大的值的高度
        return Integer.max(left_Height,right_Height)+1;


    }
}
