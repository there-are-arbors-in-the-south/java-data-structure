public class SizeofKLevel {

    public static int sizeofKLevel(TreeNode root,int k){
        //空树，k==1,k>1都是0个
        if(root==null){
            return 0;
        }
        //只有一个结点的树
        if(root.left==null&&root.right==null){
            //k==1,1个，k>1,0个
            return k==1?1:0;
        }
        //其他情况
        if(k==1){
            return 1;
        }
        //要递归到第k层，需要递归k次，每递归一层，K的值减1，减到0，就递归到了第K层
        int leftSize=sizeofKLevel(root.left,k-1);
        int rightSize=sizeofKLevel(root.right,k-1);
        return leftSize+rightSize;

    }
}

