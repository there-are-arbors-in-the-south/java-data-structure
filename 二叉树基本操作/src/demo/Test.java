package demo;

public class Test {
    public static void main(String[] args) {
        CreateTree tree=new CreateTree();

        System.out.println("前序遍历：");
        tree.preTraverdsal( tree.createTree());
        System.out.println();
        System.out.println("中序遍历：");
        tree.inorderTraverdsal(tree.createTree());
        System.out.println();
        System.out.println("后序遍历：");
        tree.postTraverdsal(tree.createTree());
        System.out.println();
        System.out.println("(子问题思想)结点个数：");
        System.out.println(tree.size1(tree.createTree()));
        System.out.println();
        System.out.println("(遍历思想)结点个数：");
        System.out.println(tree.size2(tree.createTree()));
        System.out.println();
        System.out.println("(子问题思想)叶子结点个数：");
        System.out.println(tree.leafSize1(tree.createTree()));
        System.out.println();
        System.out.println("(遍历思想)叶子结点个数：");
        System.out.println(tree.leafSize2(tree.createTree()));
        System.out.println();
        System.out.println("(子问题思想)二叉树高度：");
        System.out.println(tree.height(tree.createTree()));

        System.out.println();
        System.out.println("(子问题思想)第K层结点个数：");
        System.out.println(tree.kLeafSize(tree.createTree(),3));

    }
}
