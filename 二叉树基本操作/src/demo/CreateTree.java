package demo;

public class CreateTree {

    static class TreeNode{
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(){

        }
        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode createTree(){
        TreeNode n1=new TreeNode(1);
        TreeNode n2=new TreeNode(2);
        TreeNode n3=new TreeNode(3);
        TreeNode n4=new TreeNode(4);
        TreeNode n5=new TreeNode(5);
        TreeNode n6=new TreeNode(6);
        TreeNode n7=new TreeNode(7);

        n1.left=n2;n1.right=n3;
        n2.left=n4;n2.right=n5;
        n3.left=n6;n3.right=n7;


        return n1;
    }

    //二叉树前序遍历

    public void preTraverdsal(TreeNode root){
        if(root==null){
            return ;
        }
        System.out.print(root.val);
        preTraverdsal(root.left);
        preTraverdsal(root.right);
    }
    //二叉树中序遍历
    public void inorderTraverdsal(TreeNode root){
        if(root==null){
            return ;
        }

        preTraverdsal(root.left);
        System.out.print(root.val);
        preTraverdsal(root.right);
    }
    //二叉树后序遍历

    public void postTraverdsal(TreeNode root){
        if(root==null){
            return ;
        }

        preTraverdsal(root.left);
        preTraverdsal(root.right);
        System.out.print(root.val);
    }

    //求二叉树的结点数(子问题思想)
    public int size1(TreeNode root ){
        if(root==null){
            return 0;
        }
        int leftSize=size1(root.left);//左树结点个数
        int rightSize=size1(root.right);//右树结点个数
        return leftSize+rightSize+1;//左+右+根
    }

    //求二叉树的结点数(遍历思想)
    public static int usedSize;//总结点个数
    public int size2(TreeNode root){
        if(root==null){
            return 0;
        }
        usedSize=usedSize+1;//遍历一个结点加1
        size2(root.left);//左树
        size2(root.right);//右树
        return usedSize;//总结点
    }
    //求二叉树的叶子结点个数（子问题思想）
    public int leafSize1(TreeNode root){
        //判断是否是空树
        if(root==null){
            return 0;
        }
        //一棵树左右子树都为空则代表是叶子
        if(root.left==null&&root.right==null){
            return 1;//返回一棵树的叶子
        }
        int left=leafSize1(root.left);//递归求所有左字树的叶子
        int right=leafSize1(root.right);//递归求所有右字树的叶子

        return left+right;//返回左右字树总叶子
    }
    //（遍历思想）求叶子个数
    public static int leaf;
    public int leafSize2(TreeNode root){
        if(root==null){
            return 0;
        }
        if(root.left==null&&root.right==null){
            leaf=leaf+1;
        }
        leafSize2(root.left);
        leafSize2(root.right);
        return leaf;
    }
    //(子问题思想)获取二叉树的高度
    public int height(TreeNode root){
        //所有左子树，右字树更高+1 为二叉树高度
        if(root==null){
            return 0;
        }
        int left=height( root.left);//所有左字数高度
        int right=height(root.right);//所有右字数高度

        return Integer.max(left,right)+1;//求左右字树更高的加根 即左加右加1为高
    }
    //(递归思想)求第K层叶子结点个数
    public int kLeafSize(TreeNode root ,int k){
        //第K层结点个数是依靠第K-1层左右孩子相加
        //注意当K=1时，没有K-1层，只有一个根，直接返回1
        if(root==null){
            return 0;
        }
        if(k==1){
            return 1;
        }
        int left=kLeafSize(root.left , k-1);
        int right=kLeafSize(root.right , k-1);
        return left+right;
    }
}
