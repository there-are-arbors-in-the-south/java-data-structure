package demo;

public class ContainsNode {
    //查找二叉树中某个元素所在的结点
    public static TreeNode nodeOf(TreeNode root,int target){

        if(root==null){
            return null;
        }
        if(root.val==target){
            return root;
        }

        TreeNode leftResult=nodeOf(root.left,target);
        if(leftResult!=null){
            return leftResult;
        }
        TreeNode rightResult=nodeOf(root.right,target);

        return rightResult;

    }
}
