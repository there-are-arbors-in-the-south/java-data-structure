package demo;

public class IfContains {
    //给定一个结点，判断这个结点在二叉树中是否存在

    public static boolean ifContains(TreeNode root,TreeNode target){

        if(root==null){
            return false;
        }
        //root和target指向同一个对象
        if(root==target){
            return true;
        }

        boolean leftResult=ifContains(root.left,target);
        if(leftResult){
            return leftResult;
        }
        return ifContains(root.right,target);
    }
}
