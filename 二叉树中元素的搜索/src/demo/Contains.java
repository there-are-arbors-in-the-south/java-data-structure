package demo;

public class Contains {

    //查找二叉树中是否包含某元素
    public static boolean contains(TreeNode root,int target){

        if(root==null){
            return false;
        }
        if(root.val==target){
            return true;
        }
        //去左子树找
        boolean leftResult=contains(root.left,target);
        if(leftResult){
            return leftResult;
        }
        boolean rightResult=contains(root.right,target);

            return rightResult;
    }
}
