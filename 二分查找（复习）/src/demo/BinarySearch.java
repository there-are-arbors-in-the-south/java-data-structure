package demo;

public class BinarySearch {

    public static int binarySearch(long[]array,int target){
        int size= array.length;;
        int leftIdx=0;
        int rightIdx=array.length-1;
        while(leftIdx<=rightIdx){
            int midIdx=(leftIdx+rightIdx)/2;
            if(target==array[midIdx]){
                return 1;
            }
            if(target<array[midIdx]){
                rightIdx=midIdx-1;
            }else{
                leftIdx=midIdx+1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        long[]arr={1,5,6,7,12,15,45,99};
        System.out.println(binarySearch(arr,45));
        System.out.println((binarySearch(arr,77)));

    }
}
