package demo;

public class TreeNode {
    //孩子表示法
    int val;
    TreeNode left;
    TreeNode right;

    public TreeNode(int val) {
        this.val = val;
    }
}
