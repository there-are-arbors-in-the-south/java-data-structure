package demo;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
public class ListDemo {
    public static void main(String[] args) {
        List<String> course=new ArrayList<>();

        course.add("C语言");
        course.add("Java SE");
        course.add("计算机基础");
        course.add("后端开发");
        course.add("C语言");
        System.out.println(course);
        //获取0位置的元素
        System.out.println(course.get(0));
        course.set(0,"计算机基础");
        System.out.println(course);
        //截取[1,3)
        List<String> sublist=course.subList(1,3);
        System.out.println(sublist);
        //重新构造
        List<String> course2=new ArrayList<>(course);
        System.out.println(course2);
        List<String> course4=new LinkedList<>(course);
        System.out.println(course2);

        //引用的转换
        ArrayList<String> course3=(ArrayList<String>)course2;
        LinkedList<String> course5=(LinkedList<String>)course4;
    }

}
