package demo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardGame {
    public static void main(String[] args) {
        ArrayList<Card> cardList=new ArrayList<Card>(52);
        for(String suits:new String[]{"♠","♥","♦","♣"}){
            for(int rank=1;rank<=13;rank++){
                Card card=new Card(suits,rank);
                cardList.add(card);
            }
        }
        System.out.println("一副未洗过的牌：");
        System.out.println(cardList);
        System.out.println("-------------------------------------------------------");
        System.out.println("洗过的牌：");
        //洗牌
        Collections.shuffle(cardList);//shuffle本身就是洗牌的意思，可以使用Collections.shuffle方法洗牌
        System.out.println(cardList);
        //假设有5名玩家，每人一次发3张牌
        List<Card> player1=new ArrayList<>();
        List<Card> player2=new ArrayList<>();
        List<Card> player3=new ArrayList<>();
        List<Card> player4=new ArrayList<>();
        List<Card> player5=new ArrayList<>();
        //抓牌
        for(int i=0;i<3;i++){
            Card card;
            card =cardList.remove(cardList.size()-1);
            player1.add(card);
            card =cardList.remove(cardList.size()-1);
            player2.add(card);
            card =cardList.remove(cardList.size()-1);
            player3.add(card);
            card =cardList.remove(cardList.size()-1);
            player4.add(card);
            card =cardList.remove(cardList.size()-1);
            player5.add(card);
        }
        System.out.println("---------------");
        System.out.println("每个玩家发的牌：");
        System.out.println(player1);
        System.out.println(player2);
        System.out.println(player3);
        System.out.println(player4);
        System.out.println(player5);
        System.out.println("-----------------");
        System.out.println("各自展示一张牌");
        System.out.println(player1.get(1));
        System.out.println(player2.get(1));
        System.out.println(player3.get(1));
        System.out.println(player4.get(1));
        System.out.println(player5.get(1));
        System.out.println("-----------------");

        System.out.println("将所有的牌收回");
        cardList.addAll(player1);
        cardList.addAll(player2);
        cardList.addAll(player3);
        cardList.addAll(player4);
        cardList.addAll(player5);
        System.out.println(cardList);
        System.out.println("玩家手里的牌清空");
        player1.clear();
        player2.clear();
        player3.clear();
        player4.clear();
        player5.clear();
    }


}
