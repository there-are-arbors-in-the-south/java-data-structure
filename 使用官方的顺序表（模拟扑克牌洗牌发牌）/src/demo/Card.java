package demo;

public class Card {
    public String suit;//花色
    public int rank;// 牌数：1-13
    public Card(String suit,int rank){
        this.suit=suit;
        this.rank=rank;
    }
    //重写toString方法，将A，J,Q,K表示
    @Override
    public String toString() {
        String rankS= String.valueOf(rank);
        if(rank==1){
            rankS="A";
        }else if(rank==11){
            rankS="J";
        }else if(rank==12){
            rankS="Q";
        }else if(rank==13){
            rankS="K";
        }
        return String.format("{%s,%s}",suit,rankS) ;
    }
}
